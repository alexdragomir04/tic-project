const database = require('../config/firebase').firestore()
const faker = require('faker')

const controller = {

    //blogs, my blogs, add blog, profile, settings

    findAllFromUser:async (req,res)=>{
        let blogs = []
        const userKey = req.query.userKey
        console.log(userKey)
        let promise = database
            .collection("texts")
            .where('user','==',userKey)
            .get()
            .then(snapShot => {
                if(snapShot.empty){
                    return;
                }
                snapShot.forEach(doc => {
                    blogs.push(doc.data())
                })
            })
            .catch(err => {
                console.log("error getting documents", err);
            })

        Promise.resolve(promise)
            .then(() => {
                res.status(200).send(JSON.stringify(blogs))
            })
            .catch(err => {
                res.status(500).send(err)
            })

    },
    findAll: async (req, res) => {
        let blogs = []
        let promise = database
            .collection("texts")
            .get()
            .then(snapShot => {
                snapShot.forEach(doc => {
                    blogs.push(doc.data())
                })
            })
            .catch(err => {
                console.log("error getting documents", err);
            })

        Promise.resolve(promise)
            .then(() => {
                res.status(200).send(JSON.stringify(blogs))
            })
            .catch(err => {
                res.status(500).send(err)
            })

    },
    findOne: async (req, res) => {
            const blogKey = req.query.blogKey
            let myBlog = {}
            let promise = database
                .doc('/texts/'+blogKey)
                .get()
                .then(doc => {
                        if (doc.exists) {
                            myBlog = doc.data()
                        }
                })
                .catch(err => {
                    console.log("error getting document with id " + blogKey, err);
                })
            Promise.resolve(promise)
            .then(()=>{
                res.status(200).send(JSON.stringify(myBlog))
            })
            .catch(err=>{
                res.status(500).send(err)
            })

      
    },
    insert: async (req, res) => {
            const textRef = database.collection("texts")
            const blog = req.body
            const currentUser = {
                id:req.session.user,
                first_name:req.session.first_name,
                last_name:req.session.last_name
            }
            blog.user = currentUser.id
            blog.author={
                first_name:currentUser.first_name,
                last_name:currentUser.last_name
            }
            const blogId = faker.random.uuid()
            const promise = textRef.doc(blogId).set(blog)
            Promise.resolve(promise)
            .then(()=>{
                res.status(201).json({"id":blogId})
            })
            .catch(err=>{
                res.status(500).send(err)
            })     
    },
    edit: async (req, res) => {
       const blogRef = database.doc(`/texts/${req.body.blogKey}`)
       const promise = blogRef.set({
           title:req.body.title,
           text:req.body.text
       })
       Promise.resolve(promise)
       .then(()=>{
           res.status(204).json({"message":"updated"})
       })
       .catch(err=>{
           res.status(500).send(err)
       })     


    },
    delete: async (req, res) => {
        const blogRef = database.doc(`/texts/${req.body.blogKey}`)
        const promise = blogRef.delete()
        Promise.resolve(promise)
        .then(()=>{
            res.status(204).json({"message":"deleted"})
        })
        .catch(err=>{
            res.status(500).send(err)
        })     
    }

}

module.exports = controller