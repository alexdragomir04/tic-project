const database = require('../config/firebase').firestore()
const faker = require('faker')

const middleware = {
    checkLogin: async (req, res) => {
        const id = req.session.user;
        const snapshot = await database.collection('auth').get()
        const users = []
        await snapshot.forEach(user => {
            users.push({ id: user.id, user: user.data() })
        })
        const user = users.filter(user => user.id === id);
        (user.length === 0) ? res.status(403).send({ message: "Forbidden" }) : next()
    }
}

const controller = {

    register: async (req, res) => {
        const email = req.body.email;
        const password = req.body.password;
        const age = req.body.age
        const name = {
            first_name: req.body.first_name,
            last_name: req.body.last_name
        }
        const shortDescription = req.body.shortDescription

        if (!email.match('(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])'))
            res.status(400).send({ message: "Invalid Email" });
        else if (!password.length > 4)
            res.status(400).send({ message: "Invalid password" });

        const authRef = database.collection('auth')
        const usersRef = database.collection('users')
        const users = await authRef.get()
        const usersArray = []
        await users.forEach(user => {
            usersArray.push(user.data())
        })
        const existingUser = usersArray.filter(user => user.email === email)
        if (existingUser.length !== 0) {
            res.status(400).send({ message: `Email ${user[0].email} have already an account.` })
        } else {
            const authId = faker.random.uuid()
            const uuid = faker.random.uuid()
            await authRef.doc(authId).set({
                email: email,
                password: password,
                user: uuid
            })
            await usersRef.doc(uuid).set({
                age: age,
                name: name,
                shortDescription: shortDescription
            })
            res.status(201).send({ message: "Success registered" });
        }
    },
    login: async (req, res) => {
        const email = req.body.email;
        const password = req.body.password;

        const snapshot = await database.collection('auth').get()
        const users = []
        await snapshot.forEach(user => {
            users.push(user.data())
        })
        const user = users.filter(user => user.password === password && user.email === email);
        if (user.length === 0) {
            res.status(403).send({ message: "Incorrect email/password" });
        } else {
            if (req.session.user) {
                res.status(202).send({ message: "Already logged in" })
            } else {
                const user = await (await database.doc(`/users/${user[0].user}`).get()).data()
                req.session.user = user[0].user;
                req.session.first_name=user[0].first_name;
                req.session.last_nae=user[0].last_name;
                res.status(200).send({ message: "Login successfully" })
            }
        }
    },
    logout: async (req, res) => {
        req.session.reset();
        res.status(200).send({ message: "You've been logged out" });
    },
    middleware

}

module.exports = controller