const faker = require('faker')
const db = require('../config/firebase').firestore()

const controller = {
    insertFakeData: async (req, res) => {
        try {
            const textRef = db.collection("texts");
            const usersRef = db.collection("users")
            for (let i = 0; i < 10; i++) {
                const likes = faker.random.number()
                const views = faker.random.number()
                const uuid = faker.random.uuid()
                const blogId = faker.random.uuid()
                const first_name = faker.name.firstName()
                const last_name = faker.name.lastName()

                let blog = {
                    "author": {
                        "first_name": first_name,
                        "last_name": last_name
                    },
                    "data": faker.date.past(),
                    "likes": likes > views ? views : likes,
                    "text": faker.lorem.paragraph(),
                    "title": faker.lorem.words(),
                    "views": views > likes ? views : likes,
                    "user": uuid
                }

                let user = {                    
                    "name":{
                        "first_name":first_name,
                        "last_name":last_name
                    },
                    "email":faker.internet.email(),
                    "shortDescription":faker.lorem.words(),
                    "age":faker.random.number({
                        "min":18,
                        "max":30
                    })

                }
                await textRef.doc(blogId).set(blog)
                await usersRef.doc(uuid).set(user)
            }
            res.status(201).send({ "message": "created" })
        }
        catch (err) {
            console.log(err)
            res.status(500).send({ "message": "not working" })
        }
    }
}

module.exports = controller