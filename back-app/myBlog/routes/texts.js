const express = require('express');
const router = express.Router();
const textController = require('../controllers/texts')

router.get('/all',textController.findAll);
router.get('/text',textController.findOne);
router.get('/textFromUser',textController.findAllFromUser)
router.post('/insert',textController.insert)
router.put('/update',textController.edit)
router.put('/delete',textController.delete)


module.exports = router