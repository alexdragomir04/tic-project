const express = require('express');
const router = express.Router();
const texts = require('./texts');
const auth = require('./auth')
const fake = require('./faker')

/* GET home page. */

router.use('/blog',texts);
router.use('/auth',auth)
router.use('/fake',fake)

module.exports = router;
